import { useState } from 'react';
import TextField from '@mui/material/TextField';
import './Form.css';

const Form = ({putTodo}) => {
  const [value, setValue] = useState('');

  return (
      <form
          className="Form"
            onSubmit={e => {
            e.preventDefault();
            putTodo(value);
            setValue('');
          }
          }>
        <TextField
            placeholder='Enter your text...'
            className="input"
            value={value}
            onChange={e => setValue(e.target.value)}
            variant="standard"
        />
      </form>
  );
};

export default Form;