import { useState } from 'react';
import { Box, IconButton } from '@mui/material';
import DeleteIcon from '@mui/icons-material/Delete';
import Checkbox from '@mui/material/Checkbox';
import EditIcon from '@mui/icons-material/Edit';
import TextField from '@mui/material/TextField';
import './Item.css';


const Item = ({todo, index, toggleTodo, editTodo, deleteTodo, addEditTodo}) => {
  const {id, done, text, edit} = todo;

  const [editText, setEditText] = useState(text);

  const label = {inputProps: {'aria-label': 'Checkbox demo'}};

  return (
      <li className="item_wrap" key={id}>
        <Checkbox
            {...label}
            checked={done}
            onClick={() => toggleTodo(id)}
            disabled={edit}
        />
        <IconButton
            aria-label="edit"
            disabled={done}
            onClick={() => {
              editTodo(id);
            }}
        >
          <EditIcon/>
        </IconButton>
        <Box className={done ? "todo done" : "todo"}>
          {edit ?
              <form
                  className="Form"
                  onSubmit={e => {
                    e.preventDefault();
                    addEditTodo(id, editText);
                  }
                  }>
                <TextField
                    className="input"
                    value={editText}
                    onChange={e => setEditText(e.target.value)}
                    variant="standard"
                />
              </form>
              :
              editText
          }
        </Box>
        <IconButton
            aria-label="delete"
            color={done ? "error" : "default"}
            onClick={() => deleteTodo(index)}
            disabled={edit}
        >
          <DeleteIcon/>
        </IconButton>
      </li>
  );
}

export default Item;



