import Form from '../Form/Form';
import { useState } from 'react';
import ListItems from '../ListItems/ListItems';
import Filter from '../Filter/Filter';
import { Box } from '@mui/material';
import './App.css';



function App() {
  const [todos, setTodos] = useState([]);
  const [completed, setCompleted] = useState(false);
  const [showAll, setShowAll] = useState(true);
  const [current, setCurrent] = useState(false);


   const putTodo = (value) => {
    if (value) {
      setTodos([...todos, {
        id: Date.now(),
        text: value,
        done: false,
        edit: false,
      }])
    } else {
      alert('Enter your text...');
    }
  }

  const completeAllTodos = () => {
    const allTodos = todos.map(todo => {
      if (todos.filter(todo => !todo.done).length) {
        return {
          ...todo,
          done: true,
        }
      }

      return {
        ...todo,
        done: !todo.done,
      }
    });

    setTodos(allTodos);
  }

  const deleteAll = () => {
    const filterTodos = todos.filter( todo => todo.done === false);
    setTodos(filterTodos);
  }

   const toggleTodo = (id) => {
    setTodos(todos.map(todo => {
      if (todo.id !== id) {
        return todo;
      }

      return {
        ...todo,
        done: !todo.done
      }
    }))
  }

  const editTodo = (id) => {
    const newTodo = todos.filter(todo => {
      if (todo.id === id) {
        todo.edit = !todo.edit
      }
       return todo;
    })
    setTodos(newTodo);
  }

  const addEditTodo = (id, editText) => {
    const newTodo = todos.map(todo => {
      if (todo.id === id) {
        todo.edit = !todo.edit
        todo.text = editText;
      }
      return todo;
    })
    setTodos(newTodo);
  }

  const deleteTodo = (index) => {
    setTodos([...todos.slice(0, index), ...todos.slice(index + 1)]);
  }

  const currentTodos = () => {
    setCurrent(true);
    setShowAll(false);
    setCompleted(false);
  }

  const completedTodos = () => {
    setCompleted(true);
    setShowAll(false);
    setCurrent(false)
  }

  const allTodos = () => {
    setShowAll(true);
    setCompleted(false);
    setCurrent(false);
  }



  return (
    <Box className="wrapper">
      <Box className="container">
        <h1 className="title">TodoList</h1>
        <Form putTodo={putTodo}/>
        <ListItems
            todos={todos}
            completeAllTodos={completeAllTodos}
            toggleTodo={toggleTodo}
            editTodo={editTodo}
            addEditTodo={addEditTodo}
            deleteAll={deleteAll}
            deleteTodo={deleteTodo}
            completed={completed}
            showAll={showAll}
            current={current}
        />
        <Filter
            currentTodos={currentTodos}
            completedTodos={completedTodos}
            allTodos={allTodos}
        />
      </Box>
    </Box>
  );
}

export default App;
